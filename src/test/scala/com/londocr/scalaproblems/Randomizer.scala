package com.londocr.scalaproblems

import scala.util.Random

object Randomizer {

  def randomIntList(size: Int, lower: Int = 0, upper: Int = 1000): Seq[Int] = {
    (1 to size).map(_ => Random.between(lower, upper + 1)).toList
  }
}
