package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

import scala.util.Random

class P07Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $flattenNil
      |Lista definida plana $flattenDef
      |Lista definida 1 nivel $flattenDef1
      |Lista definida 2 niveles $flattenDef2
      |Lista random plana $flattenRandom
      |Lista random 1 nivel $flattenRandom1
      |Lista random 2 niveles $flattenRandom2
      |""".stripMargin

  def flattenNil = {
    val lista = List.empty[Int]
    Lists.flatten(lista) must beEmpty
  }

  def flattenDef = {
    val lista = List(1, 2, 3, 4, 5)
    Lists.flatten(lista) must beEqualTo(lista)
  }

  def flattenDef1 = {
    val lista = List(List(1, 1), 2, List(3, 5, 8))
    val expected = List(1, 1, 2, 3, 5, 8)
    Lists.flatten(lista) must beEqualTo(expected)
  }

  def flattenDef2 = {
    val lista = List(List(1, 1), 2, List(3, List(5, 8)))
    val expected = List(1, 1, 2, 3, 5, 8)
    Lists.flatten(lista) must beEqualTo(expected)
  }

  def flattenRandom = {
    val lista = Randomizer.randomIntList(10)
    Lists.flatten(lista) must beEqualTo(lista)
  }

  def flattenRandom1 = {
    val l1 = Randomizer.randomIntList(3).toList
    val n1 = Random.nextInt(10)
    val l2 = Randomizer.randomIntList(2).toList
    val l3 = Randomizer.randomIntList(4).toList
    val lista = List(l1, n1, l2, l3)
    val expected = List.empty[Int]
      .appendedAll(l1)
      .appended(n1)
      .appendedAll(l2)
      .appendedAll(l3)
    Lists.flatten(lista) must beEqualTo(expected)
  }

  def flattenRandom2 = {
    val l1 = Randomizer.randomIntList(3).toList
    val n1 = Random.nextInt(10)
    val l2 = Randomizer.randomIntList(2).toList
    val l3 = Randomizer.randomIntList(4).toList
    val lista = List(l1, n1, List(l2, l3))
    val expected = List.empty[Int]
      .appendedAll(l1)
      .appended(n1)
      .appendedAll(l2)
      .appendedAll(l3)
    Lists.flatten(lista) must beEqualTo(expected)
  }

}
