package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P17Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Split negativo
      |  Lista vacia $splitNegativoNil
      |  Lista definida $splitNegativoDef
      |  Lista random $splitNegativoRandom
      |  Build-in $splitNegativoScala
      |Split cero
      |  Lista vacia $splitCeroNil
      |  Lista definida $splitCeroDef
      |  Lista random $splitCeroRandom
      |  Build-in $splitCeroScala
      |Split positivo
      |  Lista vacia $splitPositivoNil
      |  Lista definida $splitPositivoDef
      |  Lista random $splitPositivoRandom
      |  Lista.size menor al split $splitPositivoMayor
      |  Build-in $splitPositivoScala
      |""".stripMargin

  def splitNegativoNil = {
    val lista = List.empty[Int]
    val split = -5
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEmpty)
  }

  def splitNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val split = -5
    val expected = lista
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEqualTo(expected))
  }

  def splitNegativoRandom = {
    val lista = Randomizer.randomIntList(10)
    val split = -5
    val expected = lista
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEqualTo(expected))
  }

  def splitNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val split = -5
    val (expected1, expected2) = lista.splitAt(split)
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEqualTo(expected1)) and (result2 must beEqualTo(expected2))
  }

  def splitCeroNil = {
    val lista = List.empty[Int]
    val split = 0
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEmpty)
  }

  def splitCeroDef = {
    val lista = List(1, 2, 3, 4, 5)
    val split = 0
    val expected = lista
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEqualTo(expected))
  }

  def splitCeroRandom = {
    val lista = Randomizer.randomIntList(10)
    val split = 0
    val expected = lista
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEqualTo(expected))
  }

  def splitCeroScala = {
    val lista = Randomizer.randomIntList(10)
    val split = 0
    val (expected1, expected2) = lista.splitAt(split)
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEqualTo(expected1)) and (result2 must beEqualTo(expected2))
  }

  def splitPositivoNil = {
    val lista = List.empty[Int]
    val split = 3
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEmpty) and (result2 must beEmpty)
  }

  def splitPositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val split = 3
    val (expected1, expected2) = (List(1, 2, 3), List(4, 5))
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEqualTo(expected1)) and (result2 must beEqualTo(expected2))
  }

  def splitPositivoRandom = {
    val lista = Randomizer.randomIntList(10)
    val split = 3
    val (result1, result2) = Lists.split(split, lista)
    (result1 must haveSize(split)) and (result2 must haveSize(10 - split))
  }

  def splitPositivoMayor = {
    val lista = Randomizer.randomIntList(10)
    val split = 30
    val expected = lista
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEqualTo(expected)) and (result2 must beEmpty)
  }

  def splitPositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val split = 3
    val (expected1, expected2) = lista.splitAt(split)
    val (result1, result2) = Lists.split(split, lista)
    (result1 must beEqualTo(expected1)) and (result2 must beEqualTo(expected2))
  }
}
