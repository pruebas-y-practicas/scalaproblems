package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P15Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $duplicateNNil
      |Lista definida 1 $duplicateNDef1
      |Lista definida 2 $duplicateNDef2
      |Lista random $duplicateNRandom
      |""".stripMargin

  def duplicateNNil = {
    val lista = List.empty[Int]
    val n = 100
    Lists.duplicateN(n, lista) must beEmpty
  }

  def duplicateNDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    val n = 5
    val expected = lista.size * n
    Lists.duplicateN(n, lista) must haveSize(expected)
  }

  def duplicateNDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val n = 3
    val expected = lista.size * n
    Lists.duplicateN(n, lista) must haveSize(expected)
  }

  def duplicateNRandom = {
    val lista = Randomizer.randomIntList(10)
    val n = 4
    val expected = lista.size * n
    Lists.duplicateN(n, lista) must haveSize(expected)
  }
}
