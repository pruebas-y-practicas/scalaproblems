package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P20Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Remove at negativo
      |  Lista vacia $removeAtNegativoNil
      |  Lista definida $removeAtNegativoDef
      |  Lista random $removeAtNegativoRandom
      |  Build-in $removeAtNegativoScala
      |Remove at cero
      |  Lista vacia $removeAtCeroNil
      |  Lista definida $removeAtCeroDef
      |  Lista random $removeAtCeroRandom
      |  Build-in $removeAtCeroScala
      |Remove at positivo
      |  Lista vacia $removeAtPositivoNil
      |  Lista definida $removeAtPositivoDef
      |  Lista random $removeAtPositivoRandom
      |  Lista.size menor al removeAt $removeAtPositivoMayor
      |  Build-in $removeAtPositivoScala
      |""".stripMargin

  def removeAtNegativoNil = {
    val lista = List.empty[Int]
    val removeAt = -5
    Lists.removeAt(removeAt, lista) must beEmpty
  }

  def removeAtNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val removeAt = -2
    val expected = lista
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtNegativoRandom = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = -2
    val expected = lista
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = -2
    val (l1, l2) = lista.splitAt(removeAt)
    val expected = l1 ++ l2
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtCeroNil = {
    val lista = List.empty[Int]
    val removeAt = 0
    Lists.removeAt(removeAt, lista) must beEmpty
  }

  def removeAtCeroDef = {
    val lista = List(1, 2, 3, 4, 5)
    val removeAt = 0
    val expected = List(2, 3, 4, 5)
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtCeroRandom = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = 0
    val expected = lista.tail
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtCeroScala = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = 0
    val (l1, l2) = lista.splitAt(removeAt)
    val expected = l1 ++ l2.drop(1)
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtPositivoNil = {
    val lista = List.empty[Int]
    val removeAt = 3
    Lists.removeAt(removeAt, lista) must beEmpty
  }

  def removeAtPositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val removeAt = 3
    val expected = List(1, 2, 3, 5)
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }

  def removeAtPositivoRandom = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = 3
    val result = Lists.removeAt(removeAt, lista)
    result must haveSize(9)
  }

  def removeAtPositivoMayor = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = 30
    val expected = lista
    val result = Lists.removeAt(removeAt, lista)
    result must beEqualTo(expected)
  }

  def removeAtPositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val removeAt = 3
    val (l1, l2) = lista.splitAt(removeAt)
    val expected = l1 ++ l2.drop(1)
    Lists.removeAt(removeAt, lista) must beEqualTo(expected)
  }
}
