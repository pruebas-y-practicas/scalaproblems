package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P11Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $encodeModifiedNil
      |Lista definida simple $encodeModifiedDef1
      |Lista definida procesable $encodeModifiedDef2
      |""".stripMargin

  def encodeModifiedNil = {
    val lista = List.empty[Int]
    Lists.encodeModified(lista) must beEmpty
  }

  def encodeModifiedDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = lista
    Lists.encodeModified(lista) must beEqualTo(expected)
  }

  def encodeModifiedDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val expected = List((4, 1), 2, (2, 3), (2, 1), 4, (4, 5))
    Lists.encodeModified(lista) must beEqualTo(expected)
  }
}
