package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P06Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $isPalindromeNil
      |Lista definida 1 $isPalindromeDef1
      |Lista definida 2 $isPalindromeDef2
      |Lista definida 3 $isPalindromeDef3
      |Lista random 1 $isPalindromeRandom1
      |Lista random 2 $isPalindromeRandom2
      |Lista random 3 $isPalindromeRandom3
      |""".stripMargin

  def isPalindromeNil = {
    val lista = List.empty[Int]
    Lists.isPalindrome(lista) must beTrue
  }

  def isPalindromeDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    Lists.isPalindrome(lista) must beFalse
  }

  def isPalindromeDef2 = {
    val lista = List(1, 2, 3, 4, 5, 4, 3, 2, 1)
    Lists.isPalindrome(lista) must beTrue
  }

  def isPalindromeDef3 = {
    val lista = List(1, 2, 3, 4, 5, 5, 4, 3, 2, 1)
    Lists.isPalindrome(lista) must beTrue
  }

  def isPalindromeRandom1 = {
    val lista = Randomizer.randomIntList(10)
    Lists.isPalindrome(lista) must beFalse
  }

  def isPalindromeRandom2 = {
    val listaTemp = Randomizer.randomIntList(10)
    val lista = listaTemp ++ listaTemp.reverse
    Lists.isPalindrome(lista) must beTrue
  }

  def isPalindromeRandom3 = {
    val listaTemp = Randomizer.randomIntList(10)
    val lista = listaTemp ++ List(45) ++ listaTemp.reverse
    Lists.isPalindrome(lista) must beTrue
  }

}
