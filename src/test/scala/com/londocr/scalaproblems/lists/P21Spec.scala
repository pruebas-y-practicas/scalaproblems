package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P21Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Insert at negativo
      |  Lista vacia $insertAtNegativoNil
      |  Lista definida $insertAtNegativoDef
      |  Lista random $insertAtNegativoRandom
      |  Build-in $insertAtNegativoScala
      |Insert at cero
      |  Lista vacia $insertAtCeroNil
      |  Lista definida $insertAtCeroDef
      |  Lista random $insertAtCeroRandom
      |  Build-in $insertAtCeroScala
      |Insert at positivo
      |  Lista vacia $insertAtPositivoNil
      |  Lista definida $insertAtPositivoDef
      |  Lista random $insertAtPositivoRandom
      |  Lista.size menor al insertAt $insertAtPositivoMayor
      |  Build-in $insertAtPositivoScala
      |""".stripMargin
      
  val valueToAdd = 50

  def insertAtNegativoNil = {
    val lista = List.empty[Int]
    val insertAt = -5
    val expected = List(valueToAdd)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val insertAt = -2
    val expected = valueToAdd +: lista
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtNegativoRandom = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = -2
    val expected = valueToAdd +: lista
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = -2
    val (l1, l2) = lista.splitAt(insertAt)
    val expected = l1 ++ (valueToAdd +: l2)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtCeroNil = {
    val lista = List.empty[Int]
    val insertAt = 0
    val expected = List(valueToAdd)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtCeroDef = {
    val lista = List(1, 2, 3, 4, 5)
    val insertAt = 0
    val expected = List(valueToAdd, 1, 2, 3, 4, 5)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtCeroRandom = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = 0
    val expected = valueToAdd +: lista
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtCeroScala = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = 0
    val (l1, l2) = lista.splitAt(insertAt)
    val expected = l1 ++ (valueToAdd +: l2)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtPositivoNil = {
    val lista = List.empty[Int]
    val insertAt = 3
    val expected = List(valueToAdd)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtPositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val insertAt = 3
    val expected = List(1, 2, 3, valueToAdd, 4, 5)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtPositivoRandom = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = 3
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must haveSize(11)
  }

  def insertAtPositivoMayor = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = 30
    val expected = lista :+ valueToAdd
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }

  def insertAtPositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val insertAt = 3
    val (l1, l2) = lista.splitAt(insertAt)
    val expected = l1 ++ (valueToAdd +: l2)
    val result = Lists.insertAt(insertAt, valueToAdd, lista)
    result must beEqualTo(expected)
  }
}
