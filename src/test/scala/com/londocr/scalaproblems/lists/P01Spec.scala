package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P01Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista definida debería retornar el último $lastDef
      |Lista random con conocido al final debería devolver el conocido $lastRandom
      |Lista vacía deberia ser None $lastNil
      |Build-in $lastScala
      |""".stripMargin

  def lastDef = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = 5
    Lists.last(lista) must beSome(expected)
  }

  def lastRandom = {
    val lista = Randomizer.randomIntList(10)
    val expected = 5
    Lists.last(lista.appended(expected)) must beSome(expected)
  }

  def lastNil = {
    val lista = List.empty[Int]
    Lists.last(lista) must beNone
  }

  def lastScala = {
    val lista = Randomizer.randomIntList(10)
    val expected = lista.last
    Lists.last(lista) must beSome(expected)
  }

}
