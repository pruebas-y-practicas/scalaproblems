package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P19Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Rotate negativo
      |  Lista vacia $rotateNegativoNil
      |  Lista definida $rotateNegativoDef
      |  Lista.size menor al rotate $rotateNegativoMenor
      |  Build-in $rotateNegativoScala
      |Rotate cero
      |  Lista vacia $rotateCeroNil
      |  Lista definida $rotateCeroDef
      |  Lista random $rotateCeroRandom
      |  Build-in $rotateCeroScala
      |Rotate positivo
      |  Lista vacia $rotatePositivoNil
      |  Lista definida $rotatePositivoDef
      |  Lista.size menor al rotate $rotatePositivoMayor
      |  Build-in $rotatePositivoScala
      |""".stripMargin

  def rotateNegativoNil = {
    val lista = List.empty[Int]
    val rotate = -5
    Lists.rotate(rotate, lista) must beEmpty
  }

  def rotateNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val rotate = -2
    val expected = List(4, 5, 1, 2, 3)
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotateNegativoMenor = {
    val lista = Randomizer.randomIntList(10)
    val rotate = -30
    val expected = lista
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotateNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val rotate = -2
    val (l1, l2) = lista.splitAt(lista.size + rotate)
    val expected = l2 :++ l1
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotateCeroNil = {
    val lista = List.empty[Int]
    val rotate = 0
    Lists.rotate(rotate, lista) must beEmpty
  }

  def rotateCeroDef = {
    val lista = List(1, 2, 3, 4, 5)
    val rotate = 0
    val expected = lista
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotateCeroRandom = {
    val lista = Randomizer.randomIntList(10)
    val rotate = 0
    val expected = lista
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotateCeroScala = {
    val lista = Randomizer.randomIntList(10)
    val rotate = 0
    val (l1, l2) = lista.splitAt(rotate)
    val expected = l2 :++ l1
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotatePositivoNil = {
    val lista = List.empty[Int]
    val rotate = 3
    Lists.rotate(rotate, lista) must beEmpty
  }

  def rotatePositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val rotate = 3
    val expected = List(4, 5, 1, 2, 3)
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotatePositivoMayor = {
    val lista = Randomizer.randomIntList(10)
    val rotate = 30
    val expected = lista
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }

  def rotatePositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val rotate = 3
    val (l1, l2) = lista.splitAt(rotate)
    val expected = l2 :++ l1
    Lists.rotate(rotate, lista) must beEqualTo(expected)
  }
}
