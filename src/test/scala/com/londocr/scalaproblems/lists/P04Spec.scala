package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P04Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $lengthNil
      |Lista definida $lengthDef
      |Lista random $lengthRandom
      |Build-in $lengthScala
      |""".stripMargin

  def lengthNil = {
    val lista = List.empty[Int]
    val expected = 0
    Lists.length(lista) must beEqualTo(expected)
  }

  def lengthDef = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = 5
    Lists.length(lista) must beEqualTo(expected)
  }

  def lengthRandom = {
    val lista = Randomizer.randomIntList(10)
    val expected = 10
    Lists.length(lista) must beEqualTo(expected)
  }

  def lengthScala = {
    val lista = Randomizer.randomIntList(10)
    val expected = lista.size
    Lists.length(lista) must beEqualTo(expected)
  }

}
