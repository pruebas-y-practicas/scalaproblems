package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P13Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $encodeDirectNil
      |Lista definida simple $encodeDirectDef1
      |Lista definida procesable $encodeDirectDef2
      |""".stripMargin

  def encodeDirectNil = {
    val lista = List.empty[Int]
    Lists.encodeDirect(lista) must beEmpty
  }

  def encodeDirectDef1 = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = lista.map(n => (1, n))
    Lists.encodeDirect(lista) must beEqualTo(expected)
  }

  def encodeDirectDef2 = {
    val lista = List(1, 1, 1, 1, 2, 3, 3, 1, 1, 4, 5, 5, 5, 5)
    val expected = List((4, 1), (1, 2), (2, 3), (2, 1), (1, 4), (4, 5))
    Lists.encodeDirect(lista) must beEqualTo(expected)
  }
}
