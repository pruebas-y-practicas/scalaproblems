package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P14Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $duplicateNil
      |Lista definida $duplicateDef
      |Lista random $duplicateRandom
      |""".stripMargin

  def duplicateNil = {
    val lista = List.empty[Int]
    Lists.duplicate(lista) must beEmpty
  }

  def duplicateDef = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = lista.size * 2
    Lists.duplicate(lista) must haveSize(expected)
  }

  def duplicateRandom = {
    val lista = Randomizer.randomIntList(10)
    val expected = lista.size * 2
    Lists.duplicate(lista) must haveSize(expected)
  }
}
