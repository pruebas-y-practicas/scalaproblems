package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P16Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Drop negativo
      |  Lista vacia $dropNegativoNil
      |  Lista definida $dropNegativoDef
      |  Lista random $dropNegativoRandom
      |  Build-in $dropNegativoScala
      |Drop cero
      |  Lista vacia $dropCeroNil
      |  Lista definida $dropCeroDef
      |  Lista random $dropCeroRandom
      |  Build-in $dropCeroScala
      |Drop positivo
      |  Lista vacia $dropPositivoNil
      |  Lista definida $dropPositivoDef
      |  Lista random $dropPositivoRandom
      |  Lista.size menor al drop $dropPositivoMayor
      |  Build-in $dropPositivoScala
      |""".stripMargin

  def dropNegativoNil = {
    val lista = List.empty[Int]
    val drop = -5
    Lists.drop(drop, lista) must beEmpty
  }

  def dropNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val drop = -5
    val expected = lista
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropNegativoRandom = {
    val lista = Randomizer.randomIntList(10)
    val drop = -5
    val expected = lista
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val drop = -5
    val expected = lista.drop(drop)
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropCeroNil = {
    val lista = List.empty[Int]
    val drop = 0
    Lists.drop(drop, lista) must beEmpty
  }

  def dropCeroDef = {
    val lista = List(1, 2, 3, 4, 5)
    val drop = 0
    val expected = lista
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropCeroRandom = {
    val lista = Randomizer.randomIntList(10)
    val drop = 0
    val expected = lista
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropCeroScala = {
    val lista = Randomizer.randomIntList(10)
    val drop = 0
    val expected = lista.drop(drop)
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropPositivoNil = {
    val lista = List.empty[Int]
    val drop = 3
    Lists.drop(drop, lista) must beEmpty
  }

  def dropPositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val drop = 3
    val expected = List(4, 5)
    Lists.drop(drop, lista) must beEqualTo(expected)
  }

  def dropPositivoRandom = {
    val lista = Randomizer.randomIntList(10)
    val drop = 3
    val expected = lista.size - drop
    Lists.drop(drop, lista) must haveSize(expected)
  }

  def dropPositivoMayor = {
    val lista = Randomizer.randomIntList(10)
    val drop = 30
    Lists.drop(drop, lista) must beEmpty
  }

  def dropPositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val drop = 3
    val expected = lista.drop(drop)
    Lists.drop(drop, lista) must beEqualTo(expected)
  }
}
