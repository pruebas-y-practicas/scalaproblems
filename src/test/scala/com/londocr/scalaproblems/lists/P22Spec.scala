package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.Lists
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P22Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |from < to
      |  from < 0, to < 0
      |    define resulting list $s1LessLessDef
      |    build-in range $s1LessLessScala
      |  from < 0, to = 0
      |    define resulting list $s1LessEqualDef
      |    build-in range $s1LessEqualScala
      |  from < 0, to > 0
      |    define resulting list $s1LessGreatDef
      |    build-in range $s1LessGreatScala
      |  from = 0, to > 0
      |    define resulting list $s1EqualGreatDef
      |    build-in range $s1EqualGreatScala
      |  from > 0, to > 0
      |    define resulting list $s1GreatGreatDef
      |    build-in range $s1GreatGreatScala
      |from = to
      |  from < 0, to < 0
      |    define resulting list $s2LessLessDef
      |    build-in range $s2LessLessScala
      |  from = 0, to = 0
      |    define resulting list $s2EqualEqualDef
      |    build-in range $s2EqualEqualScala
      |  from > 0, to > 0
      |    define resulting list $s2GreatGreatDef
      |    build-in range $s2GreatGreatScala
      |from > to
      |  from < 0, to < 0
      |    define resulting list $s3LessLessDef
      |    build-in range $s3LessLessScala
      |  from = 0, to < 0
      |    define resulting list $s3EqualLessDef
      |    build-in range $s3EqualLessScala
      |  from > 0, to < 0
      |    define resulting list $s3GreatLessDef
      |    build-in range $s3GreatLessScala
      |  from > 0, to = 0
      |    define resulting list $s3GreatEqualDef
      |    build-in range $s3GreatEqualScala
      |  from > 0, to > 0
      |    define resulting list $s3GreatGreatDef
      |    build-in range $s3GreatGreatScala
      |""".stripMargin

  def s1LessLessDef = {
    val start = -9
    val end = -4
    val expected = List(-9, -8, -7, -6, -5, -4)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1LessLessScala = {
    val start = -9
    val end = -4
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1LessEqualDef = {
    val start = -9
    val end = 0
    val expected = List(-9, -8, -7, -6, -5, -4, -3, -2, -1, 0)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1LessEqualScala = {
    val start = -9
    val end = 0
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1LessGreatDef = {
    val start = -9
    val end = 9
    val expected = List(-9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1LessGreatScala = {
    val start = -9
    val end = 9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1EqualGreatDef = {
    val start = 0
    val end = 9
    val expected = List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1EqualGreatScala = {
    val start = 0
    val end = 9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1GreatGreatDef = {
    val start = 4
    val end = 9
    val expected = List(4, 5, 6, 7, 8, 9)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s1GreatGreatScala = {
    val start = 4
    val end = 9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2LessLessDef = {
    val start = -9
    val end = start
    val expected = List(-9)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2LessLessScala = {
    val start = -9
    val end = start
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2EqualEqualDef = {
    val start = 0
    val end = start
    val expected = List(0)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2EqualEqualScala = {
    val start = 0
    val end = start
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2GreatGreatDef = {
    val start = 4
    val end = start
    val expected = List(4)
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s2GreatGreatScala = {
    val start = 4
    val end = start
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEqualTo(expected)
  }

  def s3LessLessDef = {
    val start = -4
    val end = -9
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3LessLessScala = {
    val start = -4
    val end = -9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3EqualLessDef = {
    val start = 0
    val end = -9
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3EqualLessScala = {
    val start = 0
    val end = -9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatLessDef = {
    val start = 9
    val end = -9
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatLessScala = {
    val start = 9
    val end = -9
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatEqualDef = {
    val start = 9
    val end = 0
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatEqualScala = {
    val start = 9
    val end = 0
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatGreatDef = {
    val start = 9
    val end = 4
    val result = Lists.range(start, end)
    result must beEmpty
  }

  def s3GreatGreatScala = {
    val start = 9
    val end = 4
    val expected = start to end
    val result = Lists.range(start, end)
    result must beEmpty
  }

}
