package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P02Spec extends Specification {

  override def is: SpecStructure =
    s2"""
      |Lista vacía $penNil
      |Lista con 1 elemento $pen1Element
      |Lista definida $penDef
      |Lista random $penRandom
      |Build-in $penScala
      |""".stripMargin

  def penNil = {
    val lista = List.empty[Int]
    Lists.penultimo(lista) must beNone
  }

  def pen1Element = {
    val lista = List(1)
    Lists.penultimo(lista) must beNone
  }

  def penDef = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = 4
    Lists.penultimo(lista) must beSome(expected)
  }

  def penRandom = {
    val lista = Randomizer.randomIntList(10)
    val expected = 5
    Lists.penultimo(lista.appendedAll(Seq(expected, expected + 54))) must beSome(expected)
  }

  def penScala = {
    val lista = Randomizer.randomIntList(10)
    val expected = lista.takeRight(2).head
    Lists.penultimo(lista) must beSome(expected)
  }

}
