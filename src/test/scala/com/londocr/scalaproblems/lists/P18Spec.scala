package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P18Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Slice negativo
      |  Lista vacia $sliceNegativoNil
      |  Lista definida $sliceNegativoDef
      |  Lista random $sliceNegativoRandom
      |  Build-in $sliceNegativoScala
      |Slice positivo
      |  Lista vacia $slicePositivoNil
      |  Lista definida $slicePositivoDef
      |  Lista random $slicePositivoRandom
      |  Lista.size menor al start $slicePositivoStartMayor
      |  Lista.size menor al end $slicePositivoEndMayor
      |  Build-in $slicePositivoScala
      |""".stripMargin

  def sliceNegativoNil = {
    val lista = List.empty[Int]
    val (start, end) = (-2, -1)
    val result = Lists.slice(start, end, lista)
    result must beEmpty
  }

  def sliceNegativoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val (start, end) = (-2, -1)
    val result = Lists.slice(start, end, lista)
    result must beEmpty
  }

  def sliceNegativoRandom = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (-2, -1)
    val result = Lists.slice(start, end, lista)
    result must beEmpty
  }

  def sliceNegativoScala = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (-2, -1)
    val result = Lists.slice(start, end, lista)
    val expected = lista.slice(start, end)
    result must beEqualTo(expected)
  }

  def slicePositivoNil = {
    val lista = List.empty[Int]
    val (start, end) = (2, 4)
    val result = Lists.slice(start, end, lista)
    result must beEmpty
  }

  def slicePositivoDef = {
    val lista = List(1, 2, 3, 4, 5)
    val (start, end) = (2, 4)
    val expected = List(3, 4)
    val result = Lists.slice(start, end, lista)
    result must beEqualTo(expected)
  }

  def slicePositivoRandom = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (2, 4)
    val expected = end - start
    val result = Lists.slice(start, end, lista)
    (result must haveSize(expected)) and (result.headOption must beEqualTo(lista.drop(start).headOption))
  }

  def slicePositivoEndMayor = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (9, 40)
    val expected = lista.size - start
    val result = Lists.slice(start, end, lista)
    result must haveSize(expected)
  }

  def slicePositivoStartMayor = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (30, 40)
    val result = Lists.slice(start, end, lista)
    result must beEmpty
  }

  def slicePositivoScala = {
    val lista = Randomizer.randomIntList(10)
    val (start, end) = (2, 4)
    val result = Lists.slice(start, end, lista)
    val expected = lista.slice(start, end)
    result must beEqualTo(expected)
  }
}
