package com.londocr.scalaproblems.lists

import com.londocr.scalaproblems.{Lists, Randomizer}
import org.specs2.Specification
import org.specs2.specification.core.SpecStructure

class P05Spec extends Specification {
  override def is: SpecStructure =
    s2"""
      |Lista vacía $reverseNil
      |Lista definida $reverseDef
      |Build-in $reverseScala
      |""".stripMargin

  def reverseNil = {
    val lista = List.empty[Int]
    val expected = lista
    Lists.reverse(lista) must beEqualTo(expected)
  }

  def reverseDef = {
    val lista = List(1, 2, 3, 4, 5)
    val expected = List(5, 4, 3, 2, 1)
    Lists.reverse(lista) must beEqualTo(expected)
  }

  def reverseScala = {
    val lista = Randomizer.randomIntList(10)
    val expected = lista.reverse
    Lists.reverse(lista) must beEqualTo(expected)
  }

}
