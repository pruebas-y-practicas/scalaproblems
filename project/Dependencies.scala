import sbt._

private object Versions {
  val specs2 = "4.19.2"
}

object Dependencies {
  val specs2 = "org.specs2" %% "specs2-core" % Versions.specs2 % Test
}
