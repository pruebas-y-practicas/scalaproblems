import Dependencies._

ThisBuild / version := "0.1"

ThisBuild / organization := "com.londocr"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "ScalaProblems"
  ).settings(
    libraryDependencies ++= Seq(
      specs2
    )
  )
